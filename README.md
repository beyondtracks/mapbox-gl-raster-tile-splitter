# mapbox-gl-raster-tile-splitter

A Mapbox GL JS [transformRequest](https://docs.mapbox.com/mapbox-gl-js/api/#map) function to split a single raster source request to a range of raster tile endpoints.

## Usage

Add via npm with

```sh
yarn add @beyondtracks/mapbox-gl-raster-tile-splitter
```

```js
import rasterTileSplitter from '@beyondtracks/mapbox-gl-raster-tile-splitter'
```

Add via script with

```html
<script src='dist/rasterTileSplitter.js'></script>
```

### Example
```js
var rasterSources = {
    type: 'Feature',
    properties: {
        url: 'https://example.com/secondarySource/{z}/{x}/{y}'
    },
    geometry: {
        type: 'Polygon',
        coordinates: ...
    }
};

new mapboxgl.Map({
    style: {
        version: 8,
        sources: {
            raster: {
                type: 'raster',
                tiles: ['https://example.com/primarySource/{z}/{x}/{y}'],
                tileSize: 256
            }
        },
        layers: ...
    },
    transformRequest: rasterTileSplitter(sources, 'https://example.com/primarySource')
});
```

This will result in the raster source using the primarySource, exect for tiles that fall within the bounds of the polygon used for the secondarySource.
