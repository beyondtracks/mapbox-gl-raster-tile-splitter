import whichPolygon from 'which-polygon'
import SphericalMercator from '@mapbox/sphericalmercator'

/*
 * The tile URL must end in `/{z}/{x}/{y}[.ext]?`
 *
 * @param {Object} sources A GeoJSON of source footprints, with optional properties minzoom, maxzoom, rank, id, and mandatory properties, url
 * @param {String} urlStart A token used as the start of a URL which is to be transformed
 * @returns {Function} Mapbox GL JS transformRequest function
 *
 * @example
 *
 *   new mapboxgl.Map({
 *     container: 'map',
 *     style: {
 *       version: 8,
 *       sources: {
 *         raster: {
 *           type: 'raster',
 *           tileSize: 256,
 *           tiles: [ 'https://example.com/tiles/{z}/{x}/{y}' ]
 *         }
 *       },
 *       layers: [
 *         {
 *           id: 'raster',
 *           type: 'raster',
 *           source: 'raster',
 *           minzoom: 0,
 *           maxzoom: 22
 *         }
 *       ]
 *     },
 *     transformRequest: rasterTileSplitter({
 *       type: 'FeatureCollection',
 *       features: [
 *         {
 *           type: 'Feature',
 *           properties: {
 *             minzoom: 10,
 *             url: 'https://example.com/alternate/{z}/{x}/{y}'
 *           },
 *           geometry: {
 *             type: 'Polygon',
 *             coordinates: [ [ ... ] ]
 *           }
 *         }
 *       ]
 *     }, 'https://example.com/')
 *   })
 */
export default (sources, urlStart) => {
    const query = whichPolygon(sources)
    const merc = new SphericalMercator({
        size: 256
    });

    return function (url, resourceType) {
        if (resourceType === 'Tile' && url.startsWith(urlStart)) {
            const urlParts = url.split('/')
            const z = urlParts[urlParts.length - 3]
            const x = urlParts[urlParts.length - 2]
            const y = urlParts[urlParts.length - 1].split('.')[0]

            const tileBounds = merc.bbox(x, y, z)

            const results = query.bbox(tileBounds)

            const matchingResults = results
                // filter out any sources not within the min/maxzoom
                .filter((result) => {
                    if (!result.minzoom) {
                        result.minzoom = 0
                    }
                    if (!result.maxzoom) {
                        result.maxzoom = 32
                    }

                    return z >= result.minzoom && z <= result.maxzoom
                })
                // sort by rank
                .sort((a, b) => {
                    return a.rank > b.rank
                })

            if (matchingResults.length) {
                const bbox3857 = merc.bbox(x, y, z, false, '900913')

                const result = matchingResults[0];

                return {
                    url: result.url
                    .replace('{z}', z)
                    .replace('{x}', x)
                    .replace('{y}', y)
                    .replace('{proj}', 'EPSG:3857')
                    .replace('{width}', '256')
                    .replace('{height}', '256')
                    .replace('{bbox}', bbox3857.join(','))
                }
            }
        }
    }
}
