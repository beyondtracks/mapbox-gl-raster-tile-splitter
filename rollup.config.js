// rollup.config.js
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
  input: 'index.js',
  output: {
    file: 'dist/rasterTileSplitter.js',
    name: 'rasterTileSplitter',
    format: 'iife'
  },

  plugins: [
    resolve(),
    commonjs()
  ]
};
